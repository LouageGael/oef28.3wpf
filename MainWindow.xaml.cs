﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oef28._3wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Docent> lijstDocenten = new List<Docent>();
        private List<Vak> lijstVakken = new List<Vak>();
        public static List<Vak> cLijstVakken = new List<Vak>();
        public MainWindow()
        {
            InitializeComponent();
            lijstVakken.Add(new Vak("Basis c#",11,"LOK1"));
            lijstVakken.Add(new Vak("Advanced c#", 13, "LOK1"));
            lijstVakken.Add(new Vak("Web", 12, "LOK7"));
            lijstVakken.Add(new Vak("GIT", 2, "LOK9"));
            var myQVakken = lijstVakken.OrderBy(x => x.Beschrijving);
            foreach(var item in myQVakken)
            {
                cbVakken.Items.Add(item.Beschrijving);
            }
            cLijstVakken = lijstVakken;
        }
        private void btnDocentToevoegen_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtDocentnaam.Text))
            {
                if(!txtDocentnaam.Text.Any(char.IsDigit))
                {
                    bool docentBestaat = false;
                    foreach(var item in lijstDocenten)
                    {
                        if(item.Naam == txtDocentnaam.Text)
                        {
                            docentBestaat = true;
                        }
                    }
                    if(!docentBestaat)
                    {
                        cbDocenten.Items.Clear();
                        lijstDocenten.Add(new Docent(txtDocentnaam.Text));
                        foreach (var item in lijstDocenten)
                        {
                            cbDocenten.Items.Add(item.Naam);
                        }
                        txtDocentnaam.Text = string.Empty;
                    } else
                    {
                        MessageBox.Show("Docent al in database.");
                    }
                } else
                {
                    MessageBox.Show("Docentnaam mag geen numerieke waarde bevatten.");
                }
            } else
            {
                MessageBox.Show("Voeg eerst een docent toe.", "Warning",MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnVoegToe_Click(object sender, RoutedEventArgs e)
        {
            //doce
           
            if (cbDocenten.SelectedIndex != -1)
            {
                //vakken cb
                if (cbVakken.SelectedIndex != -1)
                {
                    for (int i = 0; i < lijstDocenten.Count; i++)
                    {
                        if(lijstDocenten[i].Naam.Equals(cbDocenten.SelectedValue.ToString()))
                        {
                            lijstDocenten[i].Addvak(lijstVakken[cbVakken.SelectedIndex]);
                            lsDocentenGegevens.Items.Clear();
                            lsDocentenGegevens.Items.Add(lijstDocenten[i].ToString());
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selecteer eerst een vak.");
                }
            }
            else
            {
                MessageBox.Show("Selecteer eerst een docent.");
            }
        }
        private void btnVerwijder_Click(object sender, RoutedEventArgs e)
        {
            //doce
            if (cbDocenten.SelectedIndex != -1)
            {
                //vakken cb
                if (cbVakken.SelectedIndex != -1)
                {
                    for (int i = 0; i < lijstDocenten.Count; i++)
                    {
                        if (lijstDocenten[i].Naam.Equals(cbDocenten.SelectedValue.ToString()))
                        {
                            lijstDocenten[i].RemoveVak(lijstVakken[cbVakken.SelectedIndex]);
                            lsDocentenGegevens.Items.Clear();
                            lsDocentenGegevens.Items.Add(lijstDocenten[i].ToString());
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selecteer eerst een vak.");
                }
            }
            else
            {
                MessageBox.Show("Selecteer eerst een docent.");
            }
        }
        private void cbDocenten_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lsDocentenGegevens.Items.Clear();
            if(cbDocenten.SelectedIndex != -1)
            {
                lsDocentenGegevens.Items.Add(lijstDocenten[cbDocenten.SelectedIndex].ToString());
            }
        }

        private void btnVakkenToevegen_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtLesuren.Text) && !string.IsNullOrWhiteSpace(txtVak.Text))
            {
                if(int.TryParse(txtLesuren.Text, out  int lesuren))
                {
                    if (cbLeslokalen.SelectedIndex != -1)
                    {
                        bool vakAlInLijst = false;
                        foreach (var item in lijstVakken)
                        {
                            if (item.Beschrijving.Equals(txtVak.Text))
                            {
                                vakAlInLijst = true;
                            }
                        }
                        if (!vakAlInLijst)
                        {
                            string[] leslok = cbLeslokalen.SelectedItem.ToString().Split(':');
                            lijstVakken.Add(new Vak(
                             txtVak.Text,
                             lesuren,
                             leslok[1]
                        ));
                            cbVakken.Items.Clear();
                            foreach (var item in lijstVakken)
                            {
                                cbVakken.Items.Add(item.Beschrijving);
                            }
                            txtLesuren.Text = string.Empty;
                            txtVak.Text = string.Empty;

                        } else
                        {
                            MessageBox.Show("Vak is al in lijst.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Selecteer eerst een leslokaal.", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                } else  {
                    MessageBox.Show("Tekstveld lesuren waarde moet numeriek zijn.","Error",MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } else
            {
                MessageBox.Show("Niet alle vakken tekstvelden zijn ingevuld.", "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnToonVakken_Click(object sender, RoutedEventArgs e)
        {
            FrmVakken frm = new FrmVakken();
            frm.Show();
        }
    }
    public class Vak
    {
        private string _beschrijving;
        private int _lesuren;
        private string _leslokaal;

        public Vak() { }
        public Vak(string beschrijving, int lesuren, string leslokaal)
        {
            _beschrijving = beschrijving;
            _lesuren = lesuren;
            _leslokaal = leslokaal;
        }
        public override bool Equals(object obj)
        {
            bool isSame = false;
            if((obj== null) || !this.GetType().Equals(obj.GetType()))
            {
                isSame = false;
            } else
            {
                Vak v = (Vak)obj;
                isSame = v.Beschrijving == Beschrijving;
            }
            return isSame;
        }
        public override string ToString() => $"{Beschrijving} - {Lesuren} - {Leslokaal}";
        public string Beschrijving { get => _beschrijving; set => _beschrijving = value; }
        public int Lesuren { get => _lesuren; set => _lesuren = value; }
        public string Leslokaal { get => _leslokaal; set => _leslokaal = value; }
    }
    class Docent : Vak
    {
        private string _naam;
        private List<Vak> _vakken = new List<Vak>();

        public Docent() { }
        public Docent(string naam)
        {
            _naam = naam;
        }

        public void Addvak(Vak vak)
        {
            _vakken.Add(vak);
        }
        public void RemoveVak(Vak vak)
        {
            for(int i = 0; i < _vakken.Count; i++)
            {
                if(_vakken[i].Equals(vak))
                {
                    _vakken.RemoveAt(i);
                }
            }
        }
        public override string ToString()
        {
            string res = "";
            var myQ = _vakken.OrderBy(x => x.Lesuren);
            foreach(var item in myQ)
            {
                res += item.ToString() + Environment.NewLine;
            }
            return res;
        }
        public string Naam { get => _naam; set => _naam = value; }
        private List<Vak> Vakken { get => _vakken; set => _vakken = value; }
    }
}
