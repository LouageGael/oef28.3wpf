﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace oef28._3wpf
{
    /// <summary>
    /// Interaction logic for FrmVakken.xaml
    /// </summary>
    public partial class FrmVakken : Window
    {
        public FrmVakken()
        {
            InitializeComponent();
            foreach (var item in MainWindow.cLijstVakken)
            {
                lsVakken.Items.Add(item.ToString());
            }
            txtZoekOpBeschrijving.Text = "Plaats u zoekopdracht hier...";
        }
      
        private void btknVakkenAZ_Checked(object sender, RoutedEventArgs e)
        {
            var myQAZ = MainWindow.cLijstVakken.OrderBy(x => x.Beschrijving);
            UpdateList(myQAZ.ToList());
        }

        private void btknVakkenZA_Checked(object sender, RoutedEventArgs e)
        {
          
            var myQZA = MainWindow.cLijstVakken.OrderByDescending(x => x.Beschrijving);
            UpdateList(myQZA.ToList());
        }

        private void btknLesuren09_Checked(object sender, RoutedEventArgs e)
        {
            var myQAZ = MainWindow.cLijstVakken.OrderBy(x => x.Lesuren);
            UpdateList(myQAZ.ToList());
        }

        private void btknLesuren90_Checked(object sender, RoutedEventArgs e)
        {
            var myQZA = MainWindow.cLijstVakken.OrderByDescending(x => x.Lesuren);
            UpdateList(myQZA.ToList());
        }

        private void btknLesLokaalAZ_Checked(object sender, RoutedEventArgs e)
        {
           
        }

        private void btknLesLokaalZA_Checked(object sender, RoutedEventArgs e)
        {
          
        }

        private void btnZoekOpBeschrijving_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtZoekOpBeschrijving.Text))
            {
                var myQ = MainWindow.cLijstVakken.Where(x => x.Beschrijving == txtZoekOpBeschrijving.Text);
              
                bool correcteInput = false;
                foreach (var item in myQ)
                {
                    if(item.Beschrijving == txtZoekOpBeschrijving.Text)
                    {
                        correcteInput = true;
                    }
                }
                if (correcteInput)
                {
                    UpdateList(myQ.ToList());
                    txtZoekOpBeschrijving.Text = "Plaats u zoekopdracht hier...";
                } else
                {
                    txtZoekOpBeschrijving.Text = "Plaats u zoekopdracht hier...";
                    MessageBox.Show("Zoekopdracht niet gevonden probeer iets anders.","Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else 
            {
                MessageBox.Show("Geen waarde ingegeven");
            }
            
        }
        private void UpdateList(List<Vak> vak)
        {
            lsVakken.Items.Clear();
            foreach (var item in vak)
            {
                lsVakken.Items.Add(item.ToString());
            }
        }
    }
}
