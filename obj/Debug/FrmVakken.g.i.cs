﻿#pragma checksum "..\..\FrmVakken.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "70F18156BCDA5E3D50514E087189227EB0767FE5E0136971F511CE7568ED3C12"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using oef28._3wpf;


namespace oef28._3wpf {
    
    
    /// <summary>
    /// FrmVakken
    /// </summary>
    public partial class FrmVakken : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox btknVakkenAZ;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox btknVakkenZA;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox btknLesuren09;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox btknLesuren90;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lsVakken;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtZoekOpBeschrijving;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\FrmVakken.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnZoekOpBeschrijving;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/oef28.3wpf;component/frmvakken.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FrmVakken.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btknVakkenAZ = ((System.Windows.Controls.CheckBox)(target));
            
            #line 24 "..\..\FrmVakken.xaml"
            this.btknVakkenAZ.Checked += new System.Windows.RoutedEventHandler(this.btknVakkenAZ_Checked);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btknVakkenZA = ((System.Windows.Controls.CheckBox)(target));
            
            #line 25 "..\..\FrmVakken.xaml"
            this.btknVakkenZA.Checked += new System.Windows.RoutedEventHandler(this.btknVakkenZA_Checked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btknLesuren09 = ((System.Windows.Controls.CheckBox)(target));
            
            #line 26 "..\..\FrmVakken.xaml"
            this.btknLesuren09.Checked += new System.Windows.RoutedEventHandler(this.btknLesuren09_Checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btknLesuren90 = ((System.Windows.Controls.CheckBox)(target));
            
            #line 27 "..\..\FrmVakken.xaml"
            this.btknLesuren90.Checked += new System.Windows.RoutedEventHandler(this.btknLesuren90_Checked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lsVakken = ((System.Windows.Controls.ListBox)(target));
            return;
            case 6:
            this.txtZoekOpBeschrijving = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.btnZoekOpBeschrijving = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\FrmVakken.xaml"
            this.btnZoekOpBeschrijving.Click += new System.Windows.RoutedEventHandler(this.btnZoekOpBeschrijving_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

